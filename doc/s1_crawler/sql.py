# -*- coding: utf-8 -*-

from datetime import date
from random import randint
import random
import mysql.connector
import datetime
import time
import math
import json
import os

import re

from requests.models import stream_decode_response_unicode



cur = None

users = dict()
tags = dict()

def get_str_sha1_secret_str(res:str):
    import hashlib
    """
    使用sha1加密算法，返回str加密后的字符串
    """
    sha = hashlib.sha1(res.encode('utf-8'))
    encrypts = sha.hexdigest()
    return encrypts


    
    
def cursorRes(cur):
    result = []
    for res in cur:
        result.append(res)
    return result

def get_uid(uname, avator):
    if uname in users:
        return users.get(uname)

    sha1 = get_str_sha1_secret_str(uname)

    s = "select * from user where email=%s;"
    cur.execute(s, (sha1 + "@yxaa.com",))
    res = cursorRes(cur)
    if len(res) != 0:
        users[uname] = res[0][0]
        return users.get(uname)

    s = "insert into user(name, password, register_time, email, avator) values (%s, unhex(%s), %s, %s, %s);"
    params = (uname, sha1, "2012-3-26 22:31", sha1 + "@yxaa.com", avator)
    print(params)
    cur.execute(s, params)
    cur.execute("SELECT @@IDENTITY")
    id = cursorRes(cur)[0][0]
    users[uname] = id
    return id

def get_tagid(tagname):
    if tagname in tags:
        return tags.get(tagname)

    s = "select * from tag where name=%s;"
    cur.execute(s, (tagname,))
    res = cursorRes(cur)
    if len(res) != 0:
        tags[tagname] = res[0][0]
        return tags.get(tagname)


    s = "insert into tag(name) values (%s);"
    params = (tagname,)
    print(params)
    cur.execute(s, params)
    cur.execute("SELECT @@IDENTITY")
    id = cursorRes(cur)[0][0]
    tags[tagname] = id
    return id

def Posttag(postid, tagid):
    print(tagid)
    s = "insert into post_tag(tagid, postid) values(%s,%s);"
    cur.execute(s, (tagid, postid))
    s = "update tag set posts = posts + 1 where tagid = %s;"
    cur.execute(s, (tagid,))

def get_id(cur):
    cur.execute("SELECT @@IDENTITY")
    return cursorRes(cur)[0][0]

def insert_post(post):
    print(post["title"])
    comments = post["comments"]
    comments_len = len(comments)
    uid = get_uid(comments[0][0], comments[0][4])
    ctime = comments[0][2]
    s = "insert into post (title, creater_uid, create_time, visit, comments) values(%s,%s,%s,%s,%s);"
    params = (post["title"], uid, ctime, 0, comments_len)
    cur.execute(s, params)
    postid = get_id(cur)

    Posttag(postid, get_tagid(post["tag"]))

    for c in comments:
        print(c[0])
        
        s = "insert into comment (content, create_time, postid, creater_uid) values(%s,%s,%s,%s);"
        params = (c[3], c[2], postid, get_uid(c[0], c[4]))
        cur.execute(s, params)


files = set()
while True:
    for f in os.listdir("res"):
        if f in files:
            continue
        print("new file " + f)
        print("connecting sql...")
        cnx = mysql.connector.connect(user='root', password='sasa',
                                host='47.112.165.182',
                                database='sunny_bbs',
                                charset='utf8',
                                use_unicode=True)
        cur = cnx.cursor()
        cur.execute('SET NAMES utf8mb4')
        cur.execute("SET CHARACTER SET utf8mb4")
        cur.execute("SET character_set_connection=utf8mb4")

        files.add(f)
        with open("res\\" + f, "r") as f:
            post = json.loads(f.read().replace('''\ufffd\ufffd\u02be: \ufffd\ufffd\ufffd\u07f1\ufffd\ufffd\ufffd\u05b9\ufffd\ufffd\u027e\ufffd\ufffd \ufffd\ufffd\ufffd\ufffd\ufffd\u0536\ufffd\ufffd\ufffd\ufffd\ufffd''', 'no content'))
            insert_post(post)
        print("commiting...")
        cur.close()
        cnx.commit()
    time.sleep(5)

# cnx = mysql.connector.connect(user='root', password='sasa',
#                         host='127.0.0.1',
#                         database='sunny_bbs',
#                         charset='utf8')
# cur = cnx.cursor()
# cur.execute('SET NAMES utf8mb4')
# cur.execute("SET CHARACTER SET utf8mb4")
# cur.execute("SET character_set_connection=utf8mb4")
# cur.execute("show variables like 'character_set_%';")
# print(cursorRes(cur))
# get_uid('🍊range', 'https://avatar.saraba1st.com/data/avatar/000/52/69/60_avatar_middle.jpg')