from bs4 import BeautifulSoup
import requests
from lxml import etree
from time import sleep
import json
import os
import subprocess
import math
from queue import Queue
import threading
session = requests.session()

temp_dir = "temp"
res_dir = "res"

if not os.path.exists(temp_dir):
    os.makedirs(temp_dir)


if not os.path.exists(res_dir):
    os.makedirs(res_dir)


session.request(url="http://bbs.saraba1st.com/2b/member.php?mod=logging&action=login&loginsubmit=yes&infloat=yes&lssubmit=yes&inajax=1", method="get", params={
        "fastloginfield": "username",
        "username": "wesdrtfg",
        "password": "",
        "quickforward": "yes",
        "handlekey": "ls",
})

def get_posts(url):
    html = session.get(url).content
    posts = []
    for i in etree.HTML(html).xpath('//a[@class="s xst"]'):
        posts.append((i.text, i.get('href').split('-')[1]))
    return posts

def get_page(id):
    url = "https://www.saraba1st.com/2b/thread-" + id + "-1-1.html"
    print(url)
    html = etree.HTML(session.get(url).content)
    title = html.xpath("/html/body//div[@class=\"pgs mbm cl \"]//span")[0].get("title")
    if title == None:
        return 1
    pages = int(title.replace("共 ", "").replace(" 页", ""))
    print("have %d page" % pages)
    return pages

def get_comments(id):
    pages = get_page(id) #min(get_page(id), 300)
    clist = []
    for pg in range(1, pages + 1):
        url = "https://www.saraba1st.com/2b/thread-" + id +"-" + str(pg) + "-1.html"
        print('in page ' + str(pg) + " " + url)
        content = session.get(url).content
        html = etree.HTML(content)
        comments = html.xpath('/html/body/div[8]/div[4]/div[2]/div[starts-with(@id, "post_")]')
        print("comments " + str(len(comments)))
        for i in comments:
            print(i.get('id'))
            x = i.xpath(".//div[@class=\"authi\"]/a")
            if len(x) == 0:
                continue
            uname = x[0].text
            print("uname " + uname)
            nlist = i.xpath(".//div[@class=\"t_fsz\"]")
            if len(nlist) == 0:
                content = 'no content'
            else:
                content = etree.tostring(nlist[0]).decode("utf-8")
            time = i.xpath(".//div[@class=\"authi\"]/em")[0].text.replace("发表于 ", "")
            print("time " + time)
            if len(i.xpath('.//div[@class="avatar"]//img')) == 0:
                avator = None
            else:
                avator = i.xpath('.//div[@class="avatar"]//img')[0].get('src')
            print("avator " + str(avator))
            uid = i.xpath(".//div[@class=\"authi\"]/a")[0].get("href").replace('space-uid-', '').replace('.html', '')
            print("uid " + uid)
            clist.append((uname, i.get('id'), time, content, avator, uid))
        sleep(1)
    return clist

def post_detail(id):

    url = "https://www.saraba1st.com/2b/thread-" + id + "-1-1.html"
    html = etree.HTML(session.get(url).content)
    tag = html.xpath('.//td[@class="plc ptm pbn vwthd"]//h1[@class="ts"]/a')[0].text[1:-1]
    title = html.xpath('.//td[@class="plc ptm pbn vwthd"]//h1[@class="ts"]/span')[0].text
    print(tag + " " + title)

    comments = get_comments(id)
    _comments = []
    works = []
    for c in comments:
        works.append((c[1], c[3]))
    ret = dict()
    def multih2d(w):
        res = html2md(w[0], w[1])
        ret[w[0]] = res

    multiwork(multih2d, works)

    for c in comments:
        _comments.append((c[0], c[1], c[2], ret.get(c[1]), c[4], c[5]))
        print("convert %s to md" % c[1])
    return {
        "title": title,
        "tag": tag,
        "comments": _comments
    }

def html2md(name, content):
    input = os.path.join(temp_dir, name + ".in")
    out = os.path.join(temp_dir, name + ".out")
    if not os.path.exists(out):
        with open(input, 'w') as f:
            f.write(content)
        subprocess.run(["node", "a.js", input, out])
    with open(out, 'r', encoding='utf-8') as f:
        return f.read()


def multiwork(worker, works, max = 16):
    q = Queue()
    for w in works:
        q.put(w)
    
    def thread():
        while q.qsize() > 0:
            w = q.get()
            worker(w)
    threads = []
    for i in range(0, max):
        threads.append(threading.Thread(target=thread))
        threads[-1].start()
    for i in threads:
        i.join()



for i in range(1, 5):
    url = "https://www.saraba1st.com/2b/forum-6-%d.html" % i
    print("pg %d" % i)
    posts = get_posts(url)
    for p in posts:
        print(p)
        res_file = os.path.join(res_dir, p[1] + ".json")
        if os.path.exists(res_file):
            continue
        res = post_detail(p[1])
        with open(res_file, "w", encoding="utf-8") as f:
            f.write(json.dumps(res))



# with open("a.json", "w") as f:
#     f.write(json.dumps(post_detail("1909927", "")))

