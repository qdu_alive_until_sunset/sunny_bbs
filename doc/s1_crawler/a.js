const TurndownService = require('turndown')
const fs = require('fs');

let args = process.argv.splice(2)
let input = args[0]
let out = args[1]
let s = fs.readFileSync(input).toString("utf-8")
var turndownService = new TurndownService()
const md = turndownService.turndown(s)
fs.writeFileSync(out, md)