# API

查找帖子

```
GET api/post?filter={uid,tagid}&filterId=233&pageIndex=0&pageSize={30,50,100}&order={asc,desc}
```
reply(列表)
```js
{
    Post: {
        postid: 233,
        title: 233,
        createrUid: 233,
        createrTime:"123",
        visit: 233, //访客量
        comments: 233, //发言数
        tagList:[{
                tagid:233,
                name:"tagname"
            }
        ]
    }
}
```

新建帖子

```js
POST api/post
{
    "title":"asd",
    "content":"", //总文本内容不得超过8M
    "tags":[1,2,3,4], //帖子关联的tagid
}
```

reply
```js
{
    'postid': 233,
}
```

通过id获取帖子

```js
GET api/post/{postid}
```

删帖
```
DELETE api/post/{postid}
```
reply
```
204 NO CONTENT
```

举报帖子:
```
POST api/post/{postid}/report

{
    reason: ""
}
```

comment:

获得发言

```js
GET api/comment?filter={uid,postid}&filterId=233&pageIndex=0&pageSize={30,50,100}&order={asc,desc}
```

reply:

```js
{
    data:[
        {
            cid: 233,
            content: "",
            create_time: 123,
            postid: 233,
            creater_uid: 233.
            like: 233
        }
    ]
}
```

新建发言

```js
POST api/comment

{
    'postid': 233, //所在的post号
    'content': "", //发言内容, 限制8M以内
}
```

修改发言
```js
PATCH api/comment/{cid}

{
    'content': ""// 新内容
}
```

删除发言
```
DELETE api/comment/{cid}
```

点赞发言
```
PATCH api/comment/{cid}/like
```

用户登录

```
POST api/user/login

{
    'email': "yxa@axy"，
    'password': "" // sha1 hash的16进制字符串
}
```

用户注册
```js
POST api/user/register


{
    "name": "String", //检查最长20位
    "password": "String", //sha1 hash，前端将结果传入（16进制）
    "email": "String" //前端使用正则检查格式
}
```

reply
```js
{
    'uid': 233
}
```

修改用户信息
```js
PATCH api/user/{uid}

{
    //需要更新的字段 name/password/
    'name':'yxa2111',
    'password': 'asd',
}
```

获得用户信息
```js
GET api/user/{uid}
```

reply:
```js
{
    "uid":233,
    "name":"asdasd",
    "register_time": 233, // unix timestamp
    "last_login": 233, // unix timestamp
    "email": "ax@asd.asd",
    "permission": 0,
}
```


查找用户
```js
GET api/user?{email={}}&{last_login={TIMESTAMP}&cond={less,greater}}&{register_time={TIMESTAMP}&cond={less,greater}}&{name={}}&order={asc,desc}
```

返回一个user array

获得用户帖子收藏
```js
GET api/user/{uid}/favorite?pageIndex=0&pageSize={30,50,100}&order={asc,desc}
```

回复同查找帖子

删除用户帖子收藏

```js
DELETE api/user/{uid}/favorite/{postid}
```

tag:

新建tag
```js
POST api/tag

{
    'name': 123,
}
```

reply:
{
    'tagid': 233
}

获得tag
```js
GET api/tag/{tagid}
```

获得全部tag
```js
GET api/tag?pageIndex=0&pageSize={30,50,100}&order={asc,desc}
```

删除tag
```js
DELETE api/tag/{tagid}
```

