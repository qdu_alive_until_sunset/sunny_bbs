# sunny_bbs

#### 介绍

本项目是一款基于spring的高级校园论坛后端系统，具有良好的安全性检测、api风格设计，以及详细的单元测试和极为标准的数据字典。提供包括但不限于用户注册登录、发帖、盖楼、带标签（tag）发帖等经典校园论坛网站操作。

#### 软件架构

本项目采用spring框架开发
1.  基于spring&spring boot
2.  测试模块使用mockMVC
3.  数据库采用Mysql 8

#### 安装教程

1.  下载并安装intellij idea
2.  使用version control功能将本项目clone至本地。推荐使用idea插件市场内的gitee相关插件进行操作
3.  clone完成后，请耐心等待依赖下载完毕

#### 使用说明

1.  本项目所提供的api可于doc文件夹内查看详情。想测试其功能可直接运行测试包下的任意测试类
2.  好用啊，很好用啊

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
