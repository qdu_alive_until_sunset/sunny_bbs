package com.qdu.dream_bbs.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qdu.dream_bbs.DreamBbsApplication;
import com.qdu.dream_bbs.api.WebConfig;
import com.qdu.dream_bbs.entity.Comment;
import com.qdu.dream_bbs.entity.Post;
import com.qdu.dream_bbs.repository.CommentRepository;
import com.qdu.dream_bbs.repository.PostRepository;
import com.qdu.dream_bbs.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;

import static com.qdu.dream_bbs.utils.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(
        classes = {DreamBbsApplication.class})
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebConfig.class)
@WebAppConfiguration
@EnableAutoConfiguration
public class CommentControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockHttpSession session;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        session = new MockHttpSession();
    }

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Test
    @Transactional
    public void newCommentTest() throws Exception {
        Map<String, Object> params = new HashMap<>();
        var user = newUser(userRepository, (short) 1);
        var post = newPost(postRepository, user.getUid());
        long commentCnt = post.getComments();
        session.setAttribute("user", user);
        params.put("postid", post.getPostid());
        params.put("content", randStr(cc, 100, 200));
        params = responseJson(request(session, mockMvc, post("/api/comment"), params, true).andExpect(status().isOk()));
        assertEquals(commentCnt + 1, postRepository.findById(((Integer) params.get("postid")).longValue()).get().getComments().longValue());
        assertTrue(commentRepository.findById(((Integer) params.get("cid")).longValue()).get().getContent().equals((String) params.get("content")));
    }

    @Test
    @Transactional
    public void getCommentByPostidTest() throws Exception {
        Map<String, Object> params = new HashMap<>();
        var user = newUser(userRepository, (short) 1);
        session.setAttribute("user", user);

        for (int xxxx = 0; xxxx < 5; ++xxxx) {
            var post = newPost(postRepository, user.getUid());
            long visitCnt = post.getVisit();
            var v = new Vector<Comment>();
            for (int i = 0; i < 100; ++i) {
                v.add(newComment(commentRepository, user.getUid(), post.getPostid()));
            }
            params.put("filter", "postid");
            params.put("pageIndex", 0);
            params.put("pageSize", 100);
            params.put("order", "asc");
            params.put("filterId", post.getPostid());
            var lists = responseJList(request(session, mockMvc, get("/api/comment"), params, false).andExpect(status().isOk()));
            assertEquals(lists.size(), v.size());
            assertEquals(visitCnt + 1, postRepository.findById(post.getPostid()).get().getVisit().longValue());
            for (int i = 0; i < lists.size(); ++i) {
                assertEquals(v.elementAt(i).getCid().longValue(),
                        ((Integer) ((Map<String, Object>) lists.get(i)).get("cid")).longValue());
                assertEquals(v.elementAt(i).getCreaterUid().longValue(),
                        ((Integer) ((Map<String, Object>) lists.get(i)).get("createrUid")).longValue());
            }
        }
    }

    @Test
    @Transactional
    public void getCommentByUidTest() throws Exception {
        Map<String, Object> params = new HashMap<>();

        for (int xxxx = 0; xxxx < 5; ++xxxx) {
            var user = newUser(userRepository, (short) 1);
            session.setAttribute("user", user);
            var post = newPost(postRepository, user.getUid());
            var v = new Vector<Comment>();
            for (int i = 0; i < 100; ++i) {
                v.add(newComment(commentRepository, user.getUid(), post.getPostid()));
            }
            params.put("filter", "uid");
            params.put("pageIndex", 0);
            params.put("pageSize", 100);
            params.put("order", "asc");
            params.put("filterId", user.getUid());
            var lists = responseJList(request(session, mockMvc, get("/api/comment"), params, false).andExpect(status().isOk()));
            assertEquals(lists.size(), v.size());
            for (int i = 0; i < lists.size(); ++i) {
                assertEquals(v.elementAt(i).getCid().longValue(),
                        ((Integer) ((Map<String, Object>) lists.get(i)).get("cid")).longValue());
                assertEquals(v.elementAt(i).getCreaterUid().longValue(),
                        ((Integer) ((Map<String, Object>) lists.get(i)).get("createrUid")).longValue());
            }
        }
    }

    @Test
    @Transactional
    public void alterCommentTest() throws Exception {
        var u = newUser(userRepository, (short) 254);
        var post = newPost(postRepository, u.getUid());
        session.setAttribute("user", u);
        Map<String, Object> params = new HashMap<>();
        params.put("content", "你是一个，一个一个一个");
        request(session, mockMvc, patch("/api/comment/114514"), params, true).andExpect(status().isNotFound());
        params.clear();
        Vector<Comment> v = new Vector<>();
        for (int i = 0; i < 100; ++i) {
            v.add(newComment(commentRepository, u.getUid(), post.getPostid()));
            if (i % 5 == 0) {
                params.put("content", new StringBuilder(v.get(i).getContent()).reverse().toString());
                request(session, mockMvc, patch("/api/comment/" + v.lastElement().getCid()), params, true).andExpect(status().isNoContent());
            }
        }
    }

    @Test
    @Transactional
    public void alterCommentPTest() throws Exception {
        var u = newUser(userRepository, (short) 1);
        var post = newPost(postRepository, u.getUid());
        var comment = newComment(commentRepository, u.getUid(), post.getPostid());

        var u2 = newUser(userRepository, (short) 1);
        var post2 = newPost(postRepository, u2.getUid());
        var comment2 = newComment(commentRepository, u2.getUid(), post2.getPostid());

        session.setAttribute("user", u);
        Map<String, Object> params = new HashMap<>();
        params.put("content", "你是一个，一个一个一个一个");
        request(session, mockMvc, patch("/api/comment/" + comment.getCid()), params, true).andExpect(status().isNoContent());
        request(session, mockMvc, patch("/api/comment/" + comment2.getCid()), params, true).andExpect(status().isForbidden());
        u.setPermission((short) 254);
        request(session, mockMvc, patch("/api/comment/" + comment2.getCid()), params, true).andExpect(status().isNoContent());
    }

    @Test
    @Transactional
    public void deleteCommentPTest() throws Exception {
        var u = newUser(userRepository, (short) 1);
        var post = newPost(postRepository, u.getUid());
        var comment = newComment(commentRepository, u.getUid(), post.getPostid());

        var u2 = newUser(userRepository, (short) 1);
        var post2 = newPost(postRepository, u2.getUid());
        var comment2 = newComment(commentRepository, u2.getUid(), post2.getPostid());

        Map<String, Object> params = new HashMap<>();
        session.setAttribute("user", u);
        request(session, mockMvc, delete("/api/comment/" + comment.getCid()), params, false).andExpect(status().isNoContent());
        request(session, mockMvc, delete("/api/comment/" + comment2.getCid()), params, false).andExpect(status().isForbidden());
        u.setPermission((short) 254);
        request(session, mockMvc, delete("/api/comment/" + comment2.getCid()), params, false).andExpect(status().isNoContent());
    }

    @Test
    @Transactional
    public void deleteCommentTest() throws Exception {
        var u = newUser(userRepository, (short) 254);
        session.setAttribute("user", u);
        Map<String, Object> params = new HashMap<>();
        Vector<Comment> v = new Vector<>();
        request(session, mockMvc, delete("/api/comment/114514"), params, false).andExpect(status().isNotFound());
        var post = newPost(postRepository, u.getUid());
        for (int i = 0; i < 100; ++i) {
            v.add(newComment(commentRepository, u.getUid(), post.getPostid()));
            if (i % 3 == 0) {
                request(session, mockMvc, delete("/api/comment/" + v.lastElement().getCid()), params, false);
                v.remove(v.lastElement());
            }
        }
        System.out.println("Before request vector size:" + v.size());

        params.put("filter", "uid");
        params.put("filterId", u.getUid());
        params.put("pageIndex", 0);
        params.put("pageSize", 100);
        params.put("order", "asc");
        var resp = responseJList(request(session, mockMvc, get("/api/comment"), params, false).andExpect(status().isOk()));
        var set = new HashSet<Long>();
        for (var p : resp) {
            set.add(Long.valueOf((Integer) ((Map<String, Object>) p).get("cid")));
        }
        System.out.println("After request set size:" + set.size());
        assertEquals(set.size(), v.size());
        for (int i = 0; i < v.size(); ++i) {
            boolean find = false;
            if (set.contains(v.elementAt(i).getCid())) {
                find = true;
            }
            if (!find) {
                System.out.println((new ObjectMapper()).writeValueAsString(v.elementAt(i)));
            }
            Assertions.assertTrue(find);
        }
    }

    @Test
    @Transactional
    public void addLikeCommentTest() throws Exception {
        var u = newUser(userRepository, (short) 254);
        var post = newPost(postRepository, u.getUid());
        session.setAttribute("user", u);
        Map<String, Object> params = new HashMap<>();
        request(session, mockMvc, patch("/api/comment/114514111/like"), params, true).andExpect(status().isNotFound());
        Vector<Comment> v = new Vector<>();
        for (int i = 0; i < 100; ++i) {
            v.add(newComment(commentRepository, u.getUid(), post.getPostid()));
            if (i % 5 == 0) {
                request(session, mockMvc, patch("/api/comment/" + v.lastElement().getCid() + "/like"), params, true).andExpect(status().isNoContent());
            }
        }

        params.put("filter", "uid");
        params.put("filterId", u.getUid());
        params.put("pageIndex", 0);
        params.put("pageSize", 100);
        params.put("order", "asc");
        var resp = responseJList(request(session, mockMvc, get("/api/comment"), params, false).andExpect(status().isOk()));
        var v_after = new Vector<Long>();
        for (var p : resp) {
            v_after.add(Long.valueOf((Integer) ((Map<String, Object>) p).get("like")));
        }
        assertEquals(v_after.size(), v.size());
        for (int i = 0; i < 100; ++i) {
            boolean find = false;
            if ((i % 5 == 0 && v_after.elementAt(i) == v.elementAt(i).getLike() + 1) || v_after.elementAt(i) == v.elementAt(i).getLike()) {
                find = true;
            }
            if (!find) {
                System.out.println((new ObjectMapper()).writeValueAsString(v.elementAt(i)));
            }
            Assertions.assertTrue(find);
        }

    }

}
