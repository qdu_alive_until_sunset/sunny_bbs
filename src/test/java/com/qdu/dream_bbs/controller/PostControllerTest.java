package com.qdu.dream_bbs.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qdu.dream_bbs.DreamBbsApplication;
import com.qdu.dream_bbs.api.WebConfig;
import com.qdu.dream_bbs.entity.Post;
import com.qdu.dream_bbs.entity.Report;
import com.qdu.dream_bbs.entity.Tag;
import com.qdu.dream_bbs.repository.*;
import com.qdu.dream_bbs.entity.User;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import org.thymeleaf.model.IStandaloneElementTag;

import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.qdu.dream_bbs.utils.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(
        classes = {DreamBbsApplication.class})
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebConfig.class)
@WebAppConfiguration
@EnableAutoConfiguration
public class PostControllerTest {

    @Autowired
    private UserController controller;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockHttpSession session;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        session = new MockHttpSession();
    }

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private ReportRepository reportRepository;

    @Test
    @Transactional
    public void addPostTest() throws Exception {
        Map<String, Object> params = new HashMap<>();
        request(session, mockMvc, post("/api/post"), params, true).andExpect(status().isForbidden());

        //login
        var u = newUser(userRepository, (short) 1);
        session.setAttribute("user", u);

        params.put("title", "大珠小珠落玉盘");
        params.put("content", "意义是我一时一九一九八一零");

        //不带tag的建贴请求
        var resp = responseJson(request(session, mockMvc, post("/api/post"), params, true).andExpect(status().isOk()));
        //分别测post、comment、tag对象，以title、content和tagid为测试量
        //post:
        long postid = ((Integer) resp.get("postid")).longValue();
        var post = postRepository.findById(postid).get();
        assertEquals(post.getTitle(), (String) params.get("title"));

        //comment:
        var comment = commentRepository.findByPostid(postid, Pageable.unpaged()).stream().findFirst().get();
        assertEquals(comment.getContent(), (String) params.get("content"));

        //tag:
//        Long[] list = new Long[]{Long.valueOf(111), Long.valueOf(222), Long.valueOf(333), Long.valueOf(444), Long.valueOf(555)};
        List<Long> tagList = new ArrayList<>();
        long sampleTagPosts = 114514;
        for (int i = 0; i < 3; i++) {
            Tag tmpTag = newTag(tagRepository);
            if (i == 0) {
                sampleTagPosts = tmpTag.getPosts();
            }
            tagList.add(tmpTag.getTagid());
        }

        params.put("tags", tagList);

        //带新建tag的建贴请求
        resp = responseJson(request(session, mockMvc, post("/api/post"), params, true).andExpect(status().isOk()));
        System.out.println(resp);
//        resp.forEach((k, v)-> System.out.println("key:"+k+", val:"+v));
        ((List<Object>) params.get("tags")).forEach(t -> System.out.println("tagParamTest: " + ((Long) t).longValue()));

        //需要重新获取帖子ID
        postid = ((Integer) resp.get("postid")).longValue();
        List<Tag> sampleTagList = tagRepository.findTagByPostid(postid);
        var tag1 = sampleTagList.get(0);
        var tag2 = sampleTagList.get(1);
        var tag3 = sampleTagList.get(2);
        assertEquals(tag1.getTagid(), ((List<Integer>) params.get("tags")).get(0));
        //检查tag的posts字段是否被成功更改
        assertEquals(sampleTagPosts + 1, tag1.getPosts().longValue());
        assertEquals(tag2.getTagid(), ((List<Long>) params.get("tags")).get(1));
        assertEquals(tag3.getTagid(), ((List<Integer>) params.get("tags")).get(2));

    }

    @Test
    @Transactional
    public void getPostByPostidTest() throws Exception {
        var u = newUser(userRepository, (short) 1);
        session.setAttribute("user", u);
        Vector<Post> v = new Vector<>();
        for (int i = 0; i < 100; i++) {
            v.add(newPost(postRepository, u.getUid()));
        }
        Map<String, Object> params = new HashMap<>();
        for (int i = 0; i < 100; i++) {
            if (i % 3 == 0) {
                request(session, mockMvc, get("/api/post/" + v.elementAt(i).getPostid()), params, false).andExpect(status().isOk());
            }
        }
    }

    @Test
    @Transactional
    @Rollback(value = true) //不写也无所谓，默认的
    public void getPostByUidTest() throws Exception {
        var u = newUser(userRepository, (short) 1);
        session.setAttribute("user", u);
        Vector<Post> v = new Vector<>();
        for (int i = 0; i < 100; ++i) {
            v.add(newPost(postRepository, u.getUid()));
        }
        Map<String, Object> params = new HashMap<>();
        params.put("filter", "uid");
        params.put("filterId", u.getUid());
        params.put("pageSize", 99);
        params.put("order", "asc");
        params.put("pageIndex", 0);
        request(session, mockMvc, get("/api/post"), params, false).andExpect(status().isBadRequest());
        params.put("pageSize", 50);
        for (int pg = 0; pg < 2; ++pg) {
            params.put("pageIndex", pg);

            var resp = responseJList(request(session, mockMvc, get("/api/post"), params, false).andExpect(status().isOk())).toArray();
            System.out.println("resp len " + resp.length);

            for (int i = pg * 50; i < (pg + 1) * 50; ++i) {
                boolean find = false;
                System.out.println("i " + i);
                if (((Integer) ((Map<String, Object>) resp[i - pg * 50]).get("postid")).longValue()
                        == v.elementAt(i).getPostid().longValue()) {
                    find = true;
                }
                if (!find) {
                    System.out.println((new ObjectMapper()).writeValueAsString(resp[i - pg * 50]));
                }
                assertTrue(find);
            }
        }
        for (var p : v) {
            postRepository.delete(p);
        }
        userRepository.delete(u);
    }

    @Test
    @Transactional
    public void getPostByTagidTest() throws Exception {
        var u = newUser(userRepository, (short) 1);
        session.setAttribute("user", u);
        Vector<Post> v_tagPost = new Vector<>();
        Tag tag = newTag(tagRepository);
        System.out.println("New tag is: ID:" + tag.getTagid() + ", name:" + tag.getName());
        for (int i = 0; i < 60; i++) {
            v_tagPost.add(newPostWithTag(postRepository, u.getUid(), tag));
        }
        Map<String, Object> params = new HashMap<>();
        params.put("filter", "tagid");
        params.put("filterId", tag.getTagid());
        params.put("pageSize", 30);
        params.put("order", "asc");
        for (int pg = 0; pg < 2; pg++) {
            params.put("pageIndex", pg);

            var resp = responseJList(request(session, mockMvc, get("/api/post"), params, false).andExpect(status().isOk())).toArray();
            System.out.println("resp len " + resp.length);
            for (int i = pg * 30; i < (pg + 1) * 30; i++) {
                boolean find = false;
                System.out.println(i + "th post:");
                System.out.println("resp postid:" + ((Integer) ((Map<String, Object>) resp[i - pg * 30]).get("postid")).longValue() + ", vector postid:" + v_tagPost.elementAt(i).getPostid().longValue());
                System.out.println("resp " + (Map<String, Object>) resp[i - pg * 30]);
                if (((Integer) ((Map<String, Object>) resp[i - pg * 30]).get("postid")).longValue() ==
                        v_tagPost.elementAt(i).getPostid().longValue()) {
                    find = true;
                }
                if (!find) {
                    System.out.println((new ObjectMapper()).writeValueAsString(resp[i - pg * 30]));
                }
                assertTrue(find);
                assertNotEquals(null, ((Map<String, Object>) resp[i - pg * 30]).get("tagList"));

            }
        }
    }


    @Test
    @Transactional
    public void getPostNoFilterTest() throws Exception {
        var u = newUser(userRepository, (short) 1);
        session.setAttribute("user", u);
        Vector<Post> v_tagPost = new Vector<>();
        for (int i = 0; i < 60; i++) {
            v_tagPost.add(newPost(postRepository, u.getUid()));
        }
        Map<String, Object> params = new HashMap<>();
        params.put("filter", "null");
        params.put("filterId", 233);
        params.put("pageSize", 30);
        params.put("order", "asc");
        for (int pg = 0; pg < 2; pg++) {
            params.put("pageIndex", pg);

            var resp = responseJList(request(session, mockMvc, get("/api/post"), params, false).andExpect(status().isOk())).toArray();
            System.out.println("resp len " + resp.length);
            for (int i = pg * 30; i < (pg + 1) * 30; i++) {
                boolean find = false;
                System.out.println(i + "th post:");
                System.out.println("resp postid:" + ((Integer) ((Map<String, Object>) resp[i - pg * 30]).get("postid")).longValue() + ", vector postid:" + v_tagPost.elementAt(i).getPostid().longValue());
                if (((Integer) ((Map<String, Object>) resp[i - pg * 30]).get("postid")).longValue() ==
                        v_tagPost.elementAt(i).getPostid().longValue()) {
                    find = true;
                }
                if (!find) {
                    System.out.println((new ObjectMapper()).writeValueAsString(resp[i - pg * 30]));
                }
                assertTrue(find);
            }
        }
    }

    @Test
    @Transactional
    public void DeletePostTest() throws Exception {
        var u = newUser(userRepository, (short) 254);
        session.setAttribute("user", u);
        Map<String, Object> params = new HashMap<>();
        Vector<Post> v = new Vector<>();
        request(session, mockMvc, delete("/api/post/999999"), params, false).andExpect(status().isNotFound());
        for (int i = 0; i < 100; ++i) {
            v.add(newPost(postRepository, u.getUid()));
            if (i % 3 == 0) {
                request(session, mockMvc, delete("/api/post/" + v.lastElement().getPostid()), params, false);
            }
        }

        params.put("filter", "uid");
        params.put("filterId", u.getUid());
        params.put("order", "asc");
        params.put("pageSize", 100);

        params.put("pageIndex", 0);
        var resp = responseJList(request(session, mockMvc, get("/api/post"), params, false).andExpect(status().isOk()));
        var set = new HashSet<Long>();
        for (var p : resp) {
            set.add(Long.valueOf((Integer) ((Map<String, Object>) p).get("postid")));
        }
        System.out.println(set.size());
        for (int i = 0; i < 100; ++i) {
            boolean find = false;
            if ((v.elementAt(i).isVisible() && set.contains(v.elementAt(i).getPostid())) ||
                    (!v.elementAt(i).isVisible() && !set.contains(v.elementAt(i).getPostid()))) {
                find = true;
            }
            if (!find) {
                System.out.println((new ObjectMapper()).writeValueAsString(v.elementAt(i)));
            }
            assertTrue(find);
            postRepository.delete(v.elementAt(i));
        }
        userRepository.delete(u);
    }

    @Test
    @Transactional
    public void UserFavoriteTest() throws Exception {
        var u = newUser(userRepository, (short) 254);
        session.setAttribute("user", u);
        Map<String, Object> params = new HashMap<>();
        var v = new Vector<Post>();
        for (int i = 0; i < 100; ++i)
            v.add(newPost(postRepository, u.getUid()));
        for (int i = 0; i < 100; ++i) {
            request(session, mockMvc, post("/api/user" + "/favorite/" + v.elementAt(i).getPostid()), params, false)
                    .andExpect(status().isOk());
        }
        params.put("pageIndex", 0);
        params.put("pageSize", 100);
        params.put("order", "asc");
        var lists = responseJList(request(session, mockMvc, get("/api/user/" + u.getUid() + "/favorite"), params, false)
                .andExpect(status().isOk())).toArray();
        assertEquals(100, lists.length);
        for (int i = 0; i < 100; ++i) {
            assertEquals(Integer.valueOf(v.elementAt(i).getPostid().intValue()), ((Map<String, Object>) lists[i]).get("postid"));
        }

        for (int i = 0; i < 100; ++i) {
            if (i % 2 == 0) {
                request(session, mockMvc, delete("/api/user/" + u.getUid() + "/favorite/" + v.elementAt(i).getPostid()), params, false)
                        .andExpect(status().is(204));
            }
        }
        lists = responseJList(request(session, mockMvc, get("/api/user/" + u.getUid() + "/favorite"), params, false)
                .andExpect(status().isOk())).toArray();
        assertEquals(50, lists.length);
        for (int i = 0; i < 100; ++i) {
            if (i % 2 == 1) {
                assertEquals(Integer.valueOf(v.elementAt(i).getPostid().intValue()), ((Map<String, Object>) lists[(i - 1) / 2]).get("postid"));
            }
        }

        for (int i = 0; i < 100; ++i) {
            if (i % 2 == 1) {
                request(session, mockMvc, delete("/api/user/" + u.getUid() + "/favorite/" + v.elementAt(i).getPostid()), params, false)
                        .andExpect(status().is(204));
            }
        }

        lists = responseJList(request(session, mockMvc, get("/api/user/" + u.getUid() + "/favorite"), params, false)
                .andExpect(status().isOk())).toArray();
        assertEquals(0, lists.length);
    }

    @Test
    @Transactional
    public void reportPostTest() throws Exception {
        var u = newUser(userRepository, (short) 1);
        session.setAttribute("user", u);
        Map<String, Object> paramsReport = new HashMap<>();
        paramsReport.put("reason", "便乘禁止");
        var v = new Vector<Post>();
        request(session, mockMvc, post("/api/post/114514/report"), paramsReport, false).andExpect(status().isNotFound());
        for (int i = 0; i < 100; i++)
            v.add(newPost(postRepository, u.getUid()));
        for (int i = 0; i < 100; i++) {
            if (i % 4 == 0)
                request(session, mockMvc, post("/api/post/" + v.elementAt(i).getPostid() + "/report"), paramsReport, false)
                        .andExpect(status().isNoContent());
        }

        Map<String, Object> params = new HashMap<>();

        params.put("filter", "uid");
        params.put("filterId", u.getUid());
        params.put("order", "asc");
        params.put("pageSize", 100);
        params.put("pageIndex", 0);

        var resp = responseJList(request(session, mockMvc, get("/api/post"), params, false).andExpect(status().isOk()));
        var undeleteSet = new HashSet<Long>();
        var unreportSet = new HashSet<Long>();

        //与删除相比添加了判断帖子是否已被举报的逻辑
        for (var p : resp) {
            Long postid = Long.valueOf((Integer) ((Map<String, Object>) p).get("postid"));
            undeleteSet.add(postid);
            if (reportRepository.findByPostid(postid).isEmpty())
                unreportSet.add(postid);
        }
        System.out.println("postID set's size: " + undeleteSet.size());

        for (int i = 0; i < 100; i++) {
            boolean find = false;
            if ((v.elementAt(i).isVisible() && undeleteSet.contains(v.elementAt(i).getPostid()) && unreportSet.contains(v.elementAt(i).getPostid())) ||
                    (!v.elementAt(i).isVisible() && !undeleteSet.contains(v.elementAt(i).getPostid()) && !unreportSet.contains(v.elementAt(i).getPostid()))) {
                find = true;
            }
            if (!find) {
                System.out.println((new ObjectMapper()).writeValueAsString(v.elementAt(i)));
            }
            assertTrue(find);
            postRepository.delete(v.elementAt(i));
        }
    }

}
