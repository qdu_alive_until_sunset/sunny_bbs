package com.qdu.dream_bbs.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qdu.dream_bbs.DreamBbsApplication;
import com.qdu.dream_bbs.repository.UserRepository;
import com.qdu.dream_bbs.api.WebConfig;
import com.qdu.dream_bbs.api.exception.ApiException;
import com.qdu.dream_bbs.api.exception.ErrorCode;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;


import static com.qdu.dream_bbs.utils.newUser;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import com.qdu.dream_bbs.entity.User;

//@ExtendWith(SpringExtension.class)
//@WebMvcTest(UserController.class)
//@TestPropertySource(
//        properties = {
//                "spring.datasource.url=jdbc:mysql://localhost:3306/sunny_bbs_test?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=UTF-8"
//        }
//)
@SpringBootTest(
        classes = {DreamBbsApplication.class})
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebConfig.class)
@WebAppConfiguration
@EnableAutoConfiguration
public class UserControllerTest {

    @Autowired
    private UserController controller;

    private MockHttpSession session;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        session = new MockHttpSession();
    }

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void registerTest() throws Exception {

        Map<String, Object> params = new HashMap<>();
        params.put("email", "axy@asd");

        request(post("/api/register"), params, false).andExpect(status().isBadRequest());
        params.put("name", "a");
        request(post("/api/register"), params, false).andExpect(status().isBadRequest());
        params.put("password", "ffeeffeeffeeffeeffeeffeeffeeffeeffeeffee");
        request(post("/api/register"), params, false).andExpect
                (except(e -> e instanceof ApiException && ((ApiException) e).getKey().equals("name") && ((ApiException) e).getErrcode() == ErrorCode.KEY_LEN.ordinal()));
        params.put("name", "asdas");
        params.put("password", "asd213");
        request(post("/api/register"), params, false).andExpect
                (except(e -> e instanceof ApiException && ((ApiException) e).getKey().equals("password") && ((ApiException) e).getErrcode() == ErrorCode.KEY_NOT_HEX.ordinal()));
        params.put("password", "ffeeffeeffeeffeeffeeffeeffeeffeeffeeffe");
        request(post("/api/register"), params, false).andExpect
                (except(e -> e instanceof ApiException && ((ApiException) e).getKey().equals("password") && ((ApiException) e).getErrcode() == ErrorCode.KEY_NOT_HEX.ordinal()));

        params.put("password", "ffeeffeeffeeffeeffeeffeeffeeffeeffeeff");
        request(post("/api/register"), params, false).andExpect
                (except(e -> e instanceof ApiException && ((ApiException) e).getKey().equals("password") && ((ApiException) e).getErrcode() == ErrorCode.KEY_LEN.ordinal()));

        params.put("password", "ffeeffeeffeeffeeffeeffeeffeeffeeffeeffee");
        params.put("email", "axyassdd");
        request(post("/api/register"), params, false).andExpect
                (except(e -> e instanceof ApiException && ((ApiException) e).getKey().equals("email") && ((ApiException) e).getErrcode() == ErrorCode.NOT_EMAIL.ordinal()));

        params.put("email", "asd@asf222223342sd");
        params = responseJson(request(post("/api/register"), params, false).andExpect(status().isOk()));
        System.out.println(params);
        params.put("password", "ffeeffeeffeeffeeffeeffeeffeeffeeffeeffee");
        request(post("/api/register"), params, false).andExpect
                (except(e -> e instanceof ApiException && ((ApiException) e).getKey().equals("email") && ((ApiException) e).getErrcode() == ErrorCode.KEY_IS_EXIST.ordinal()));
        userRepository.deleteById(((Integer)params.get("uid")).longValue());
        assertTrue(userRepository.findById(((Integer)params.get("uid")).longValue()).isEmpty());
    }

    @Test
    @Transactional
    public void loginTest() throws Exception {

        Map<String, Object> params = new HashMap<>();
        User user = newUser(userRepository, (short) 1);
        System.out.println(new ObjectMapper().writeValueAsString(user));
        params.put("email", "asdasfkjasjhfkasjhfk");
        params.put("password", Hex.encodeHexString(user.getPassword()));
        System.out.println(Arrays.toString(user.getPassword()));
        request(post("/api/login"), params, false).andExpect(
                status().isNotFound()
        );
        params.put("email", user.getEmail());
        params.put("password", "ccffffffffffffffffffffffffffffffffffffff");
        request(post("/api/login"), params, false).andExpect(
                status().isUnauthorized()
        );
        params.put("password", Hex.encodeHexString(user.getPassword()));
        params = responseJson(request(post("/api/login"), params, false).andExpect(
                status().isOk()
        ));
        assertEquals(((Integer) params.get("uid")).longValue(), user.getUid());
        // 这里返回的中文用户名是乱码，然而我用python测试返回的是正常的中文。怀疑是测试框架的问题
        // 指定charset之后ok
        System.out.println(params);
    }

    @Test
    @Transactional
    public void alterUserTest() throws Exception {
        Map<String, Object> params = new HashMap<>();
        User user = newUser(userRepository, (short) 1);

//        params.put("name", user.getName());
//        request(patch("/api/user/99999999"), params, true).andExpect(status().isNotFound());

        params.put("password", Hex.encodeHexString(user.getPassword()));
        params.put("email", user.getEmail());
        request(post("/api/login"), params, false).andExpect(status().isOk());
        params.put("name", new StringBuilder(user.getName()).reverse().toString());
        params.remove("email");
        request(patch("/api/user/" + user.getUid().toString()), params, true).andExpect(status().isNoContent());
//        params = responseJson(request(patch("/api/user/" + user.getUid().toString()), params, true).andExpect(status().isOk()));
        assertTrue(params.get("name").equals(userRepository.findById(user.getUid()).get().getName()));

    }

    @Test
    @Transactional
    public void alterUserPTest() throws Exception {
        Map<String, Object> params = new HashMap<>();
        User user = newUser(userRepository, (short) 1);

        User admin = new User();
        admin.setPassword(Hex.decodeHex("ffffffffffffffffffffffffffffffffffffffff"));
        admin.setName("sdasd");
        admin.setEmail("asdasxzc@ascr");
        admin.setRegisterTime(new Date());
        admin.setPermission((short) 255);
        admin = userRepository.save(admin);

        params.put("email", admin.getEmail());
        params.put("password", "ffffffffffffffffffffffffffffffffffffffff");
        request(post("/api/login"), params, false).andExpect(status().isOk());
        params.clear();
        params.put("name", new StringBuilder(user.getName()).reverse().toString());
        request(patch("/api/user/" + user.getUid()), params, true).andExpect(status().isNoContent());

        params.clear();
        params  = responseJson(request(get("/api/user/" + user.getUid()), params, false).andExpect(status().isOk()));

        assertEquals(user.getName(), params.get("name"));
    }

    @Test
    @Transactional
    public void alterUserPTest2() throws Exception {
        Map<String, Object> params = new HashMap<>();
        User user = newUser(userRepository, (short) 1);

        User admin = new User();
        admin.setPassword(Hex.decodeHex("ffffffffffffffffffffffffffffffffffffffff"));
        admin.setName("sdasd");
        admin.setEmail("asdasxzc@ascr");
        admin.setRegisterTime(new Date());
        admin.setPermission((short) 0);
        admin = userRepository.save(admin);


        params.put("email", admin.getEmail());
        params.put("password", "ffffffffffffffffffffffffffffffffffffffff");
        request(post("/api/login"), params, false).andExpect(status().isOk());
        params.clear();
        params.put("name", new StringBuilder(user.getName()).reverse().toString());
        String oldname = user.getName();
        request(patch("/api/user/" + user.getUid()), params, true).andExpect(status().isForbidden());
        assertEquals(oldname, user.getName());
    }


    @Test
    @Transactional
    public void getUser() throws Exception {
        Map<String, Object> params = new HashMap<>();
        User user = newUser(userRepository, (short) 1);

        request(get("/api/user/66666666"), params, false).andExpect(status().isNotFound());
        params = responseJson(request(get("/api/user/" + user.getUid().toString()), params, false).andExpect(status().isOk()));
        assertEquals(params.get("email"), user.getEmail());
    }



    Map<String, Object> responseJson(ResultActions actions) throws UnsupportedEncodingException, JsonProcessingException {
        var mapper = new ObjectMapper();
        return mapper.readValue(actions.andReturn().getResponse().getContentAsString(), Map.class);
    }

    public ResultMatcher except(Function<Exception, Boolean> f) {
        return (result) -> {
            if (result.getResolvedException() == null) {
                throw new RuntimeException("no exception");
            }
            result.getResolvedException().printStackTrace();
            assertTrue(f.apply(result.getResolvedException()));
        };
    }

    public ResultActions request(
            MockHttpServletRequestBuilder r,
            Map<String, Object> params,
            boolean inBody
    ) throws Exception {
        r = r.contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8").session(session);

//        Map<String, String> map = Stream.of(params).collect
//                (Collectors.toMap(data -> (String) data[0], data -> data[1].toString()));

        if (inBody) {
            String content = new ObjectMapper().writeValueAsString(params);
            r = r.content(content);
        } else {
            for (var entry: params.entrySet()) {
                r = r.param((String) entry.getKey(), entry.getValue() == null ? null : entry.getValue().toString());
            }
        }
        return mockMvc.perform(r).andDo(print());
    }
}
