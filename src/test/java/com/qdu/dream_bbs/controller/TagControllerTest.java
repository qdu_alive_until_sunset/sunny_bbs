package com.qdu.dream_bbs.controller;

import com.qdu.dream_bbs.DreamBbsApplication;
import com.qdu.dream_bbs.api.WebConfig;
import com.qdu.dream_bbs.entity.Tag;
import com.qdu.dream_bbs.repository.TagRepository;
import com.qdu.dream_bbs.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import static com.qdu.dream_bbs.utils.request;
import static com.qdu.dream_bbs.utils.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(
        classes = {DreamBbsApplication.class})
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebConfig.class)
@WebAppConfiguration
@EnableAutoConfiguration
public class TagControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockHttpSession session;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        session = new MockHttpSession();
    }

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TagRepository tagRepository;

    @Test
    @Transactional
    public void newTagTest() throws Exception {
        Map<String, Object> params = new HashMap<>();
        var user = newUser(userRepository, (short) 1);
        session.setAttribute("user", user);

        //测试tag名长度越界
        params.put("name", randStr(taggg, 65, 100));
        responseJson(request(session, mockMvc, post("/api/tag"), params, true).andExpect(status().isBadRequest()));

        params.put("name", randStr(taggg, 1, 32));
        params = responseJson(request(session, mockMvc, post("/api/tag"), params, true).andExpect(status().isOk()));
        assertEquals(tagRepository.findById(((Integer) params.get("tagid")).longValue()).get().getName(), params.get("name"));
    }

    @Test
    @Transactional
    public void getTagTest() throws Exception {
        Map<String, Object> params = new HashMap<>();
        var user = newUser(userRepository, (short) 1);
        session.setAttribute("user", user);

        var v = new Vector<Tag>();
        for (int i = 0; i < 100; i++) {
            Tag tag = newTag(tagRepository);
            v.add(tag);
        }
        for (int i = 0; i < 100; i++) {
            if (i % 3 == 0) {
                request(session, mockMvc, get("/api/tag/" + v.elementAt(i).getTagid()), params, false).andExpect(status().isOk());
            }
        }
    }

    @Test
    @Transactional
    public void getAllTagTest() throws Exception {
        Map<String, Object> params = new HashMap<>();
        var user = newUser(userRepository, (short) 1);
        session.setAttribute("user", user);

        var v = new Vector<Tag>();
        for (int i = 0; i < 100; i++) {
            v.add(newTag(tagRepository));
        }

        params.put("pageSize", 50);
        for (int pg = 0; pg < 2; pg++) {
            params.put("pageIndex", pg);
            var resp = responseJList(request(session, mockMvc, get("/api/tag"), params, false).andExpect(status().isOk())).toArray();
            for (int i = pg * 50; i < (pg + 1) * 50; i++) {
                assertEquals(((Integer) ((Map<String, Object>) resp[i - pg * 50]).get("tagid")).longValue(),
                        v.elementAt(i).getTagid().longValue());
            }
        }
    }

    @Test
    @Transactional
    public void deleteTagTest() throws Exception {
        var u = newUser(userRepository, (short) 254);
        session.setAttribute("user", u);
        Map<String, Object> params = new HashMap<>();
        Vector<Tag> v = new Vector<>();
        request(session, mockMvc, delete("/api/tag/114514"), params, false).andExpect(status().isNotFound());
        for (int i = 0; i < 100; i++) {
            v.add(newTag(tagRepository));
        }

        for (int i = 0; i < 100; i++) {
            if (i % 4 == 0) {
                request(session, mockMvc, delete("/api/tag/" + v.elementAt(i).getTagid()), params, false).andExpect(status().isNoContent());
            }
        }
    }

}
