package com.qdu.dream_bbs;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qdu.dream_bbs.entity.Comment;
import com.qdu.dream_bbs.entity.Post;
import com.qdu.dream_bbs.entity.Tag;
import com.qdu.dream_bbs.repository.CommentRepository;
import com.qdu.dream_bbs.repository.PostRepository;
import com.qdu.dream_bbs.repository.TagRepository;
import com.qdu.dream_bbs.repository.UserRepository;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.function.BiFunction;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import com.qdu.dream_bbs.entity.User;

public class utils {

    public static Map<String, Object> responseJson(ResultActions actions) throws UnsupportedEncodingException, JsonProcessingException {
        var mapper = new ObjectMapper();
        return mapper.readValue(actions.andReturn().getResponse().getContentAsString(), Map.class);
    }

    public static List<Object> responseJList(ResultActions actions) throws UnsupportedEncodingException, JsonProcessingException {
        var mapper = new ObjectMapper();
        return mapper.readValue(actions.andReturn().getResponse().getContentAsString(), List.class);
    }

    public static ResultMatcher except(Function<Exception, Boolean> f) {
        return (result) -> {
            if (result.getResolvedException() == null) {
                throw new RuntimeException("no exception");
            }
            result.getResolvedException().printStackTrace();
            assertTrue(f.apply(result.getResolvedException()));
        };
    }

    public static ResultActions request(
            MockHttpSession session,
            MockMvc mockMvc,
            MockHttpServletRequestBuilder r,
            Map<String, Object> params,
            boolean inBody
    ) throws Exception {
        r = r.contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8").session(session)
        ;

        if (inBody) {
            String content = new ObjectMapper().writeValueAsString(params);
            r = r.content(content);
        } else {
            for (var entry : params.entrySet()) {
                r = r.param((String) entry.getKey(), entry.getValue() == null ? null : entry.getValue().toString());
            }
        }
        return mockMvc.perform(r).andDo(print());
    }

    public static BiFunction<Integer, Integer, Integer> genRandInt() {
        Random r = new Random();
        return (var min, var max) -> {
            return r.nextInt(max - min) + min;
        };
    }

    public static String randStr(String t, int min, int max) {
        StringBuilder sb = new StringBuilder();
        var rand = genRandInt();
        int len = rand.apply(min, max);
        for (int i = 0; i < len; ++i) {
            sb.append(t.charAt(rand.apply(0, t.length() - 1)));
        }
        return sb.toString();
    }

    static String alphabet = "abcdefghijklmnopqrstuvwxyz";
    public static String cc = "义已逝吾亦逝忆旧倚酒跋夷陵";
    public static String taggg = "戳啦极霸矛嘛";

    public static User newUser(UserRepository userRepository, short permission) throws DecoderException {
        StringBuilder name = new StringBuilder();
        var email = new StringBuilder();
        ;
        var rand = genRandInt();
        int namelen = rand.apply(3, 20);
        for (int i = 0; i < namelen; ++i) {
            name.append(cc.charAt(rand.apply(0, cc.length() - 1)));
        }
        int elen = rand.apply(3, 10);
        for (int i = 0; i < elen; ++i) {
            email.append(alphabet.charAt(rand.apply(0, alphabet.length() - 1)));
        }
        email.append("@");
        elen = rand.apply(3, 10);
        for (int i = 0; i < elen; ++i) {
            email.append(alphabet.charAt(rand.apply(0, alphabet.length() - 1)));
        }

        User u = new User();
        u.setName(name.toString());
        u.setEmail(email.toString());
        u.setPassword(Hex.decodeHex("ffffffffffffffffffffffffffffffffffffffff"));
        u.setPermission(permission);
        u.setRegisterTime(new Date());
        return userRepository.save(u);
    }

    private static Post initPost(Long uid) {
        Post post = new Post();
        var title = randStr(cc, 10, 20);
        var content = randStr(cc, 100, 100000);
        post.setTitle(title);
        post.setCreaterUid(uid);
        post.setCreateTime(new Date());
        post.setVisible(true);
        return post;
    }

    //建不带tag的新帖
    public static Post newPost(PostRepository repo, Long uid) {
        return repo.save(initPost(uid));
    }

    //建一个带指定tag的新帖
    public static Post newPostWithTag(PostRepository postRepo, Long uid, Tag tag) {
        Post post = initPost(uid);
        List<Tag> tagList = new ArrayList<>();
        tagList.add(tag);
        post.setTagList(tagList);
        return postRepo.save(post);
    }

    public static Comment newComment(CommentRepository repo, Long createrUid, Long postid) {
        Comment comment = new Comment();
        comment.setPostid(postid);
        comment.setContent(randStr(cc, 100, 10000));
        comment.setCreaterUid(createrUid);
        comment.setCreateTime(new Date());
        return repo.save(comment);
    }

    public static Tag newTag(TagRepository repo) {
        Tag tag = new Tag();
        tag.setName(randStr(taggg, 2, 29));
        tag.setPosts(1);
        return repo.save(tag);
    }

}
