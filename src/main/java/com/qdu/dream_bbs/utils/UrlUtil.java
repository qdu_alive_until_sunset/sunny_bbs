package com.qdu.dream_bbs.utils;

@Deprecated
public class UrlUtil {

    /**
     * 处理URL拼接，避免多余的'/'
     * @param oldURL 待拼接URL
     * @param param 待拼接参数
     * @return 拼接完毕的URL
     */
    public static final String mappingUrlCat(String oldURL, String param){
        return "/"+flickChar(oldURL, '/')+"/"+flickChar(param, '/');
    }

    /**
     * 将str内首、尾的所有ch字符全部删除
     * @param str 待操作的字符串
     * @param ch 待删除字符
     * @return
     */
    public static String flickChar(String str, char ch) {
        int start, end;
        for(start = 0; start < str.length(); start++) {
            if(str.charAt(start) != ch) {
                break;
            }
        }
        for(end = str.length()-1; end > start; end--) {
            if(str.charAt(end) != ch) {
                break;
            }
        }
        return str.substring(start, end+1);
    }
}
