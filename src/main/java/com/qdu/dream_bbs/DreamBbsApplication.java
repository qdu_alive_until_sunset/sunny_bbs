package com.qdu.dream_bbs;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

import java.net.MalformedURLException;
import java.net.URL;

@SpringBootApplication
public class DreamBbsApplication {
    public static void main(String[] args) {
        SpringApplication.run(DreamBbsApplication.class, args);

    }
}
