package com.qdu.dream_bbs.entity;

import com.qdu.dream_bbs.entity.key.ReportCombineKey;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@DynamicInsert
@Table(name = "report")
public class Report implements Serializable {
    @EmbeddedId
    private ReportCombineKey id;
    private String reason;
    @Column(nullable = false)
    private Date time;

    public ReportCombineKey getId() {
        return id;
    }

    public void setId(ReportCombineKey id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
