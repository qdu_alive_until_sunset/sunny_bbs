package com.qdu.dream_bbs.entity.key;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ReportCombineKey implements Serializable {
    @Column(name = "uid")
    private Long uid;
    @Column(name = "postid")
    private Long postid;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getPostid() {
        return postid;
    }

    public void setPostid(Long postid) {
        this.postid = postid;
    }
}
