package com.qdu.dream_bbs.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

@Entity // This tells Hibernate to make a table out of this class
@DynamicUpdate
@Table(name = "user")
public class User {

    public interface WithoutPassword{};

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long uid;
    @Column(nullable = false)
    private String name;

    @Column(nullable = false, unique = true)
    private String email;

    private short Permission;

    @Column(nullable = false)
    private byte[] password;

    @Column(name="last_login")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLogin;

    @Column(name="register_time", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date registerTime;

    @Column
    private String avator;

    public String getAvator() {
        return avator;
    }

    public void setAvator(String avator) {
        this.avator = avator;
    }

    public byte[] getPassword() {
        return password;
    }

    public void setPassword(byte[] password) {
        this.password = password;
    }

    @JsonView(WithoutPassword.class)
    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    @JsonView(WithoutPassword.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @JsonView(WithoutPassword.class)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    @JsonView(WithoutPassword.class)
    public short getPermission() {
        return Permission;
    }

    public void setPermission(short permission) {
        Permission = permission;
    }
    @JsonView(WithoutPassword.class)
    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }
    @JsonView(WithoutPassword.class)
    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }
}
