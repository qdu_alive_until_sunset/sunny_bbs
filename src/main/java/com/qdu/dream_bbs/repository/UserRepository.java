package com.qdu.dream_bbs.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.qdu.dream_bbs.entity.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {
    public Optional<User> findByEmail(String email);
    @Modifying
    @Query(value = "update user set user.last_login =?2 where user.uid =?1", nativeQuery = true)
    public void updateLastLogin(Long uid, Date lastLogin);
    @Modifying
    @Query(value = "insert into user_favorite(uid, postid) values(?1, ?2)", nativeQuery = true)
    public void updateUserFavorite(Long uid, Long postid);
    @Modifying
    @Query(value = "delete from user_favorite where uid = ?1 and postid = ?2", nativeQuery = true)
    public void deleteUserFavorite(Long uid, Long postid);
}
