package com.qdu.dream_bbs.repository;

import com.qdu.dream_bbs.entity.Comment;
import com.qdu.dream_bbs.entity.Report;
import com.qdu.dream_bbs.entity.key.ReportCombineKey;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ReportRepository extends CrudRepository<Report, ReportCombineKey> {

//    Page<Report> findByUid(Long uid, Pageable pageable);

    Page<Report> findByReason(String reason, Pageable pageable);

    Page<Report> findByReasonLike(String reason, Pageable pageable);

    @Query(value = "select * from report where postid=?1", nativeQuery = true)
    Optional<Report> findByPostid(Long postid);
}
