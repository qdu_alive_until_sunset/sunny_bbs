package com.qdu.dream_bbs.controller;

import com.qdu.dream_bbs.api.annotation.*;
import com.qdu.dream_bbs.api.ownerChecks.UserCheck;
import com.qdu.dream_bbs.entity.Tag;
import com.qdu.dream_bbs.lib.LibMisc;
import com.qdu.dream_bbs.repository.TagRepository;
import com.qdu.dream_bbs.utils.SpringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.qdu.dream_bbs.utils.SpringUtil.genPageRequest;

@RestController
public class TagController {

    private static final Logger log = LoggerFactory.getLogger(CommentController.class);

    @Autowired
    private TagRepository tagRepository;

    /**
     * 检查ID是否为整数
     * 检查tag长度是否在[min,max]
     * 检查posts是否为整数
     */
    public class NewTagParamsConstraint {
        @ParamLenAnnotation.ParamLen(min = 1, max = 64)
        public String name;
        @ParamOptional
        public Integer posts;
    }

    @PostMapping(path = "/api/tag", produces = LibMisc.PRODUCE_TYPE)
    @Transactional
    @Permission(Least = 1)
    public Tag newTag(@RequestBody Map<String, Object> params) throws Exception {
        CheckListAnnotation.Check(new Class[]{NewTagParamsConstraint.class}, params, CheckListAnnotation.FilterPoilcy.FILTE_NOT_EXIST_NULL);
        Tag tag = new Tag();
        tag.setName((String) params.get("name"));
        return tagRepository.save(tag);
    }

    @GetMapping(path = "/api/tag/{tagid}", produces = LibMisc.PRODUCE_TYPE)
    @Transactional
    public Tag getTag(@PathVariable long tagid) {
        return tagRepository.findById(tagid).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Tag not found"));
    }

    @GetMapping(path = "/api/tag", produces = LibMisc.PRODUCE_TYPE)
    @Transactional
    public List<Tag> getAllTag(
            @RequestParam long pageIndex,
            @RequestParam @Contains({"30", "50", "100"}) String pageSize
    ) {
        PageRequest pageRequest = PageRequest.of((int) pageIndex, Integer.parseInt(pageSize));
        return tagRepository.findAll(pageRequest).toList();
    }

    /**
     * 检查方法同NewTagParamsConstraint
     */
    public class alterTagConstraint {
        @ParamOptional
        @CheckListAnnotation.CheckList(NewTagParamsConstraint.class)
        public String name;
        @CheckListAnnotation.CheckList(NewTagParamsConstraint.class)
        public Integer tagid;
        @CheckListAnnotation.CheckList(NewTagParamsConstraint.class)
        public Integer posts;
    }

    /**
     * 更新tag
     * 暂未启用
     */
    @PatchMapping(path = "/api/tag/{tagid}", produces = LibMisc.PRODUCE_TYPE)
    @Permission(Least = 254, Auth = UserCheck.class)
    @Transactional
    public void alterTag(
            @RequestBody Map<String, Object> properties,
            @PathVariable long tagid,
            HttpServletResponse response
    ) throws Exception {

        CheckListAnnotation.Check(new Class[]{alterTagConstraint.class}, properties, CheckListAnnotation.FilterPoilcy.FILTE_NOT_EXIST);
        Optional<Tag> newTag = tagRepository.findById(tagid);
        SpringUtil.CopyProperties(properties, newTag.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Tag not found")));

        try {
            tagRepository.save(newTag.get());
            response.setStatus(HttpStatus.NO_CONTENT.value());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * 增加tag所包括的贴数
     * 暂未启用
     */
    @PatchMapping(path = "/api/tag/{tagid}/posts", produces = LibMisc.PRODUCE_TYPE)
    @Permission(Least = 1)
    @Transactional
    public void addTagPosts(
            @RequestBody Map<String, Object> properties,
            @PathVariable long tagid,
            HttpServletResponse response
    ) throws Exception {
        Tag tag = tagRepository.findById(tagid).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Tag not found"));
        tag.setPosts(tag.getPosts() + 1);
        response.setStatus(HttpStatus.NO_CONTENT.value());
    }

    /**
     * 大概只有管理员可以删tag？
     */
    @DeleteMapping(path = "/api/tag/{tagid}", produces = LibMisc.PRODUCE_TYPE)
    @Permission(Least = 254)
    @Transactional
    public void deleteTag(@PathVariable long tagid, HttpServletResponse response) {
        tagRepository.findById(tagid).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Tag not found"));
        try {
            tagRepository.deleteById(tagid);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.setStatus(HttpStatus.NO_CONTENT.value());
    }
}
