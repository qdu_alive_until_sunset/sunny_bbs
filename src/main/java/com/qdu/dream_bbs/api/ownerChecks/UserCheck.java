package com.qdu.dream_bbs.api.ownerChecks;

import com.qdu.dream_bbs.entity.User;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Component
public class UserCheck implements OwnerCheck {

    @Override
    public boolean Check(HttpSession session, Map<String, Object> params) {
        long targetUid = Long.parseLong((String) params.get("uid"));
        if (targetUid == 0) {
            return true;
        }

        var u = Optional.ofNullable((User) session.getAttribute("user"));
        if (u.isEmpty()) {
            System.out.println("no login");
            return false;
        }
        System.out.println("targetId " + targetUid + " uid " + u.get().getUid());
        return ((User) u.get()).getUid() == targetUid;
    }
}