package com.qdu.dream_bbs.api.ownerChecks;

import com.qdu.dream_bbs.entity.User;
import com.qdu.dream_bbs.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.Optional;

@Component
public class CommentCheck implements OwnerCheck {

    @Autowired
    private CommentRepository commentRepository;

    @Override
    public boolean Check(HttpSession session, Map<String, Object> params) {
        var u = Optional.ofNullable((User) session.getAttribute("user"));
        if (u.isEmpty()) {
            System.out.println("no login");
            return false;
        }

        var uid = u.get().getUid();
        var cid = Long.parseLong(Optional.of((String) params.get("cid")).get());
        var comment = commentRepository.findById(cid);
        if (!comment.isPresent()) {
            return false;
        }

        System.out.println("comment uid " + comment.get().getCreaterUid() + " uid " + uid);
        return comment.get().getCreaterUid().longValue() == uid;
    }
}
