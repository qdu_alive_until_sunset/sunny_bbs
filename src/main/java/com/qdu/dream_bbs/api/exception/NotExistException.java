package com.qdu.dream_bbs.api.exception;

public class NotExistException extends ApiException {
    public NotExistException(String key, String msg) {
        super(key, ErrorCode.KEY_NOT_EXIST, msg);
    }
}
