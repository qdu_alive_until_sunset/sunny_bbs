create schema sunny_bbs CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
use sunny_bbs;

create table `user` (
	`uid` bigint primary key auto_increment,
    -- basic info
    `name` varchar(32) not null,
    `password` binary(20) not null, -- store sha1 https://stackoverflow.com/questions/614476/storing-sha1-hash-values-in-mysql
    `register_time` datetime not null,
    `last_login` datetime,
    `email` varchar(255) not null unique, -- https://stackoverflow.com/questions/8242567/acceptable-field-type-and-size-for-email-address
    `permission` tinyint unsigned default 0,
    `avator` varchar(255),
    -- 对于其他信息，可以之后在建一张表
    -- index
    -- 使用index，可以考虑延迟关联优化查询
    index(`name`),
    index(`email`),
    index(`permission`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- 帖子中每个人的发言，包括楼主。单独成立一个信息
create table `comment` (
	`cid` bigint primary key auto_increment,
	`content` MEDIUMTEXT not null, -- max 16 MB
    `create_time` datetime not null,
    `postid` bigint, -- 可以为空，不设外键
    `creater_uid` bigint not null,
    `like` int unsigned default 0, -- 可以对每个发言点赞
    -- index
    index(`create_time`),
    index(`postid`),
    index(`creater_uid`),
    index(`like`)
) DEFAULT CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci;
 -- 关联延迟对某个帖子的所有发言按时间顺序升序分页 
-- select * from comment, (select cid from comment where postid = $postid limit $start_pos, $pagesize) t where
-- comment.cid = t.cid order by comment.create_time asc


-- 帖子只记录基本信息
create table `post` (
	`postid` bigint primary key auto_increment,
    `title` varchar(100) not null,
    `creater_uid` bigint not null,
    `create_time` datetime not null,
    `visit` int unsigned default 0, -- 访问量
    `comments` int unsigned default 0, -- 发言量
    `visible` boolean default True,
    -- index
    index(`creater_uid`),
    index(`create_time`),
    index(`visit`),
    index(`visible`)
) DEFAULT CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci;

create table `tag` (
	`tagid` bigint primary key auto_increment,
    `name` varchar(32),
    `posts` int unsigned default 0 -- tag的贴子数
) DEFAULT CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci;

-- 帖子对应tag，因为关系是多对多，所以单独抽成一张表
create table `post_tag` (
	`tagid` bigint not null,
    `postid` bigint not null,
	primary key(postid, tagid) -- 联合主键
) DEFAULT CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci;

-- 用户帖子收藏
create table `user_favorite` (
	`uid` bigint not null,
    `postid` bigint not null,
    primary key(`uid`, `postid`)
) DEFAULT CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci;

-- 举报
create table `report` (
	`uid` bigint not null,
    `postid` bigint not null,
    primary key(`uid`, `postid`),
    `reason` varchar(32) default '',
    `time` datetime not null
) DEFAULT CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci;

-- 打印出最长的帖子
-- select *, count(*) cnt from comment group by postid order by cnt desc;

-- select * from comment, (select cid from comment where postid > 8000 limit 700000, 100) t1 where t1.cid = comment.cid order by create_time asc;

-- select count(*) from comment where postid=8001;

-- select cid, postid, creater_uid, count(cid) cnt from comment group by creater_uid order by cnt desc limit 10;

-- drop table `user`, `comment`, `post`, `tag`, `post_tag`, `user_favorite`, `report`;
-- drop schema sunny_bbs;