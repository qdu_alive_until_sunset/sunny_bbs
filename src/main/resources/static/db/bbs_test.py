from datetime import date
from random import randint
import random
import mysql.connector
import datetime
import time
import math

cnx = mysql.connector.connect(user='root', password='mysql',
                              host='127.0.0.1',
                              database='test',
                              charset='utf8')

cur = cnx.cursor()

def genText(m, n, chinese = True):
    chineseChar = '义已逝,吾亦逝，忆旧倚酒跋夷陵'
    ret = ''
    up = randint(m, n)
    for i in range(0, up):
        if randint(0, 1) == 1 and chinese:
            ret += chineseChar[randint(0, len(chineseChar) - 1)]
        else:
            ret += chr(ord('a') + randint(0, 25))
    return ret

content = genText(200, 201)

def randTime():
    return datetime.datetime.fromtimestamp(time.time() + randint(0, 10000000)).isoformat()

def genUser(n):
    users = set()
    for i in range(0, n):
        # user = {'name': genText(1,32), 'password': genText(1, 16), 'register_time':
        #  datetime.datetime.fromtimestamp(time.time() + randint(0, 10000000)).isoformat(), 'email': genText(5, 10, False)}
        statement = "insert into user(name, password, register_time, email) values (%s, unhex(sha1(%s)), %s, %s);"
        user = (genText(1,32), genText(1, 16), 
         datetime.datetime.fromtimestamp(time.time() + randint(0, 10000000)).isoformat(), genText(5, 10, False))
        try:
            cur.execute(statement, user)
        except:
            pass
        users.add(user)
    return users

def genTag(n):
    tags = []
    for i in range(0, n):
        s = "insert into tag(name) values (%s);"
        t = genText(1, 31)
        tags.append(t)
        cur.execute(s, (t,))
    return tags


tot = 0.0
cnt = 0
def genComment(postId, uid):
    s = "insert into comment (content, create_time, postid, creater_uid) values(%s,%s,%s,%s);"
    a = time.time()
    cur.execute(s, (content, randTime(), postId, uid))
    b = time.time()
    global tot
    global cnt
    cnt += 1
    tot += b - a
    if cnt % 100 == 0:
        print('gen comment %d:%d, takes %f sec, aver: %f' % (postId, uid, b - a, tot / float(cnt)))
    

def randItem(list):
    return list[randint(0, len(list) - 1)]

def genPartition(a, b, k):
    _ = [i for i in range(a, b)]
    random.shuffle(_)
    return (_[:int(len(_) * k)], _[int(len(_) * k):])

def genPostTag(postid, tagid):
    s = "insert into post_tag (tagid, postid) values (%s,%s);"
    cur.execute(s, (postid, tagid))

def genPost(n, commentNum, tagNum, userNum):
    user1, user2 = genPartition(1, userNum + 1, 0.8)
    tag1, tag2 = genPartition(1, tagNum + 1, 0.8)
    lastComment = int(commentNum * 0.2)
    commentNum -= lastComment
    lastPost = int(n * 0.8)
    n -= lastPost
    postid = 1
    while lastPost > 0 and lastComment > 0:
        if postid % 100 == 0:
            print('gen post %d' % postid)
        s = "insert into post (title, creater_uid, create_time) values(%s,%s,%s);"
        cur.execute(s, (genText(1, 31), randItem(user1), randTime()))
        genPostTag(postid, randItem(tag1))
        comments = min(lastComment, randint(1, 20))
        for j in range(0, comments):
            genComment(postid, randItem(user1))
        lastComment -= comments
        lastPost -= 1
        postid += 1
    lastPost += n
    lastComment += commentNum

    while lastComment > 0 and lastPost > 0:
        if postid % 100 == 0:
            print('gen post %d' % postid)
        s = "insert into post (title, creater_uid, create_time) values(%s,%s,%s);"
        cur.execute(s, (genText(1, 31), randItem(user2), randTime()))
        genPostTag(postid, randItem(tag2))
        comments = min(lastComment, randint(2000, max(10000, int(lastComment / 10))))
        for j in range(0, comments):
            genComment(postid, randItem(user2))
        lastComment -= comments
        lastPost -= 1
        postid += 1


def genData():
    genUser(20000)
    genTag(100)
    genPost(10000, 1000000, 100, 20000)
    cur.close()
    cnx.commit()

def cursorRes(cur):
    result = []
    for res in cur:
        result.append(res)
    return result

def runOnetest(name, execute):
    print(name)
    cur = cnx.cursor()
    a = time.time()
    execute(cur)
    res = cursorRes(cur)
    b = time.time()

    print('result:')
    for r in res[:min(len(res), 20)]:
        print(r)
    print('\nresult takes %f sec\n' % (b - a))
    cur.close()
    return res

def newQuery(statement, param = None):
    def _(cur):
        if param == None:
            cur.execute(statement)
        else:
            cur.execute(statement, param)
    return _

def test():
    runOnetest('test page', newQuery(
        'select * from comment, (select cid from comment where postid > 8000 limit 700000, 100) t1 where t1.cid = comment.cid order by create_time asc;'
    ))
    lCid, _, __, lPostid, ____, _____, lCnt = runOnetest('longest post', newQuery(
        'select *, count(*) cnt from comment group by postid order by cnt desc;'
    ))[0]
    runOnetest('查询最长帖子(%s)的所有发言，分页，时间升序' % lPostid, newQuery(
        'select * from comment, (select cid from comment where postid = %s limit %s, 100) t1 where t1.cid = comment.cid order by create_time asc;',
        (str(lPostid), int(lCnt) - 200)
    ))
    cid, pid, uid, ccnt = runOnetest('longest user, have a rest', newQuery(
        'select cid, postid, creater_uid, count(cid) cnt from comment group by creater_uid order by cnt desc limit 10;'
    ))[0]
    runOnetest('查询发言最多用户，分页, 时间降序', newQuery(
        'select * from comment, (select cid from comment where creater_uid = %s limit %s, 100) t1 where t1.cid = comment.cid order by create_time desc;',
        (int(uid), int(ccnt) - 10)
    ))

cnx.close()